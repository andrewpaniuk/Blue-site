'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    spritesmith = require('gulp.spritesmith'),
    gcmq = require('gulp-group-css-media-queries'),
    // svgstore  = require ('gulp-svgstore'),
    rename = require("gulp-rename");

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        // svg: 'build/img/sprites/',
        sprite: 'build/img/sprites/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/main.js',
        style: 'src/style/main.sass',
        img: ['src/img/**/*.*', '!src/img/icons/*.*'],
        // svg: 'src/img/**/*.svg',
        sprite: 'src/img/icons/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.+(sass|scss)',
        img: 'src/img/**/*.*',
        // svg: 'src/img/**/*.svg',
        sprite: 'src/img/icons/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Andrew_Panuk"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('sprite:build', function(){
    var fileName = 'sprite';
    var spriteData = gulp.src('src/img/icons/*.png').pipe(spritesmith({
    imgName: fileName + '.png',
    cssName: fileName + '.sass',
    cssFormat: 'sass',
    algorithm: 'top-down',
    padding: 500,
    imgPath: '../img/sprites/' + fileName + '.png',
    cssTemplate: 'sass.template.mustache',
    cssVarMap: function(sprite) {
        sprite.name = 's-' + sprite.name
    }
  }));
    spriteData.img.pipe(gulp.dest('src/img/sprites/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('src/style/partials/')); // путь, куда сохраняем стили
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass({
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(prefixer())
        .pipe(gcmq())
        .pipe(cssmin())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});



// gulp.task('svg:build', function () {
//     return gulp
//         .src(path.src.svg, { base: 'src/img/' })
//         .pipe(rename({prefix: 's-'}))
//         .pipe(svgstore())
//         .pipe(rename({basename: 'sprite'}))
//         .pipe(gulp.dest(path.build.svg));
// });

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'sprite:build',
    'image:build',
    'fonts:build'
    // 'svg:build',
]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.sprite], function(event, cb) {
        gulp.start('sprite:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    // watch([path.watch.svg], function(event, cb) {
    //     gulp.start('svg:build');
    // });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('default', ['build', 'webserver', 'watch']);
