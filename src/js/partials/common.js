$(document).ready(function() {
    // select input
    $('select').material_select();
    // tabs
    $('ul.tabs').tabs();

    // navigation
    $(".nav__burger").sideNav({
        menuWidth: 300,
        edge: 'left',
        closeOnClick: true,
        draggable: true,
        onOpen: function() {
            $('.nav__burger').addClass('active');
        },
        onClose: function() {
            $('.nav__burger').removeClass('active');
        },
    });

    // only number
    $(".numberOnly").keypress(function(e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });

    // slider

    $(".slick-arrow").html('<i class="fa fa-angle-left"></i>');
    $(".slick-arrow").html('<i class="fa fa-angle-right"></i>');
    $('.slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
        prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>'
    });

    $(document).ready(function(){
    $('.collapsible').collapsible();
  });
});

// dropdown navigation
jQuery(document).ready(function (e) {
    function t(t) {
        e(t).bind("click", function (t) {
            t.preventDefault();
            e(this).parent().fadeOut()
        })
    }
    e(".dropdown-toggle").click(function () {
        var t = e(this).parents(".button-dropdown").children(".dropdown-menu").is(":hidden");
        e(".button-dropdown .dropdown-menu").hide();
        e(".button-dropdown .dropdown-toggle").removeClass("active");
        if (t) {
            e(this).parents(".button-dropdown").children(".dropdown-menu").toggle().parents(".button-dropdown").children(".dropdown-toggle").addClass("active")
        }
    });
    e(document).bind("click", function (t) {
        var n = e(t.target);
        if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-menu").hide();
    });
    e(document).bind("click", function (t) {
        var n = e(t.target);
        if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-toggle").removeClass("active");
    })
});
